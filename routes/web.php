<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\dataController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\userList;
use App\Http\Controllers\ForgetPasswordController;
use App\Http\Controllers\userController;
use App\Http\Controllers\zoomController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Forget Password
Route::get('/ForgetPassword', [ForgetPasswordController::class, 'showForgetPasswordForm'])->name('ForgetPassword');
Route::post('/submitForgetPassword', [ForgetPasswordController::class, 'submitForgetPasswordForm'])->name('submitForgetPassword'); 
Route::get('/ResetPassword/{token}', [ForgetPasswordController::class, 'showResetPasswordForm'])->name('ResetPassword');
Route::post('/submitResetPassword', [ForgetPasswordController::class, 'submitResetPasswordForm'])->name('submitResetPassword');

//Auth
Route::get('/', [AuthController::class, 'index'])->name('login');
Route::post('/home', [AuthController::class, 'home'])->name('home');
Route::get('/home', [AuthController::class, 'home'])->name('home');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
Route::get('/registration', [AuthController::class, 'registration'])->name('registration');
Route::post('/registration', [AuthController::class, 'prosesRegistration'])->name('registration');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['cekLogin:Staff Fakultas']], function () {
    	/*
    		Route Khusus untuk role admin
    	*/
        Route::post('/fakultas', [dataController::class, 'showData'])->name('dataController');
        
        //User List
        Route::get('/userList', [userList::class, 'userList'])->name('userList');
        Route::post('/userList', [userList::class, 'userList'])->name('userList');
        Route::get('/user/hapus/{id}', [userList::class, 'hapus'])->name('hapus');

        //Data Peminjaman
        Route::get('/listPeminjaman', [dataController::class, 'listPeminjaman'])->name('listPeminjaman');
        Route::post('/listPeminjaman', [dataController::class, 'listPeminjaman'])->name('listPeminjaman');

        //Data Peminjaman Update Status
        Route::get('/peminjaman/status/{id}', [dataController::class, 'updateStatuss']);
        Route::post('/peminjaman/status/{id}', [dataController::class, 'updateStatuss']);
        Route::get('/peminjaman/statusUpdate/{id}', [dataController::class, 'updateStatus']);
        Route::post('/peminjaman/statusUpdate/{id}', [dataController::class, 'updateStatus']);

        //Tolak Peminjaman
        Route::get('/tolakPeminjaman/{id}', [dataController::class, 'tolakPeminjaman'])->name('tolakPeminjaman');
        Route::post('/tolakPeminjaman/{id}', [dataController::class, 'tolakPeminjaman'])->name('tolakPeminjaman');

        //Data Peminjaman Update Status User
        Route::get('/user/statusUpdate/{id}', [dataController::class, 'updateStatusUser'])->name('updateStatusUser');
        Route::post('/user/statusUpdate/{id}', [dataController::class, 'updateStatusUser'])->name('updateStatusUser');
        Route::get('/user/status/{id}', [dataController::class, 'StatusUser'])->name('StatusUser');
        Route::post('/user/status/{id}', [dataController::class, 'StatusUser'])->name('StatusUser');

        //Data Akun Zoom
        Route::get('/akunZoom', [zoomController::class, 'akunZoom'])->name('akunZoom');
        Route::post('/akunZoom', [zoomController::class, 'akunZoom'])->name('akunZoom');

        //Akun Zoom CRUD
        Route::get('/akunZoom/hapus/{id}', [zoomController::class, 'hapus'])->name('hapus');
        Route::get('/akunZoom/tambah', [zoomController::class, 'tambahZoom'])->name('tambahZoom');
        Route::post('/akunZoom/tambahData', [zoomController::class, 'tambahAkunZoom'])->name('tambahZoom');
        Route::get('/akunZoom/edit/{id}', [zoomController::class, 'editAkunZoom'])->name('editZoom');
        Route::post('/akunZoom/edit', [zoomController::class, 'editZoom'])->name('editZoom');


    });
    Route::group(['middleware' => ['cekLogin:Mahasiswa']], function () {
    	/*
    		Route Khusus untuk role editor
    	*/
        Route::post('/mahasiswa', [dataController::class, 'showData'])->name('dataController');

        //Data Peminjaman
        Route::get('/DataPeminjaman', [userController::class, 'dataPeminjaman'])->name('dataPeminjaman');
        Route::post('/DataPeminjaman', [userController::class, 'dataPeminjaman'])->name('dataPeminjaman');

        //Peminjaman
        Route::get('/peminjaman/hapus/{id}', [dataController::class, 'hapus'])->name('hapus');
        Route::get('/peminjaman/tambah', [dataController::class, 'tambah'])->name('tambah');
        Route::post('/peminjaman/tambahData', [dataController::class, 'tambahData'])->name('tambahData');

    });
});