<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class akun_zoom extends Model
{
    //berikan nama table
    public $table = 'akun_zoom';
    // ijinkan agar semua kolom dapat di isi dan simpan
    protected $guarded = [];
}
