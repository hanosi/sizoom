<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class data_peminjaman extends Model
{
    //berikan nama table
    public $table = 'data_peminjaman';
    // ijinkan agar semua kolom dapat di isi dan simpan
    protected $guarded = [];
}
