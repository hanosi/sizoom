<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class zoomController extends Controller
{
    public function akunZoom()
    {
        $data = DB::table('akun_zoom')->get();
        // memanggil view tambah
        return view('fakultas.zoom',['data' => $data]);
    }

    // method untuk hapus data
    public function hapus($id)
    {
        // menghapus data berdasarkan id yang dipilih
        DB::table('akun_zoom')->where('id_zoom',$id)->delete();
            
        // alihkan halaman
        return redirect('/akunZoom');
    }

    // method untuk menampilkan view form tambah
    public function tambahZoom()
    {
        
        $data = DB::table('akun_zoom')->get();
        // memanggil view tambah
        return view('action/zoom/create',['data' => $data]);
    
    }

    // method untuk menampilkan view form tambah
    public function editAkunZoom($id)
    {
        
        $data = DB::table('akun_zoom')->where('id_zoom',$id)->get();
        // memanggil view tambah
        return view('action/zoom/edit',['data' => $data]);
    
    }
    // method untuk insert data ke table peminjaman
    public function tambahAkunZoom(Request $request)
    {
        
        // insert data ke table peminjaman
        DB::table('akun_zoom')->insert([
            'id' => Null,
            'id_zoom'=> random_int(100000, 999999),
            'nama_akun' => $request->nama_akun,
            'email_zoom' => $request->email_zoom,
            'kapasitas' => $request->kapasitas,
            'tipe_akun' => $request->tipe_akun,
            'created_at'=> date("Y-m-d H:i:s"),
            'updated_at'=> date("Y-m-d H:i:s"),
        ]);

        return redirect('/akunZoom');
    }

    // update data pegawai
    public function editZoom(Request $request)
    {
        // update data pegawai
        DB::table('akun_zoom')->where('id_zoom',$request->id_zoom)->update([
            'nama_akun' => $request->nama_akun,
            'email_zoom' => $request->email_zoom,
            'kapasitas' => $request->kapasitas,
            'tipe_akun' => $request->tipe_akun,
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        // alihkan halaman ke halaman pegawai
        return redirect('/akunZoom');
    }
}
