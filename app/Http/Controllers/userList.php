<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\dataController;
use App\Models\User;

class userList extends Controller
{
    public function userList()
    {
        $data = DB::table('users')->get();
        // memanggil view tambah
        return view('fakultas/user',['data' => $data]);
    
    }

    public function dataPeminjaman()
    {
    	// mengambil data dari table peminjaman
    	$data = DB::table('data_peminjaman')
            ->join('users', 'data_peminjaman.no_identitas', '=', 'users.no_identitas')
            ->get();
 
    	// mengirim data ke view index
    	return view('/fakultas/data',['data' => $data]);
    }

    // method untuk hapus data
    public function hapus($id)
    {
        // menghapus data berdasarkan id yang dipilih
        DB::table('users')->where('id',$id)->delete();
            
        // alihkan halaman
        return redirect('/userList');
    }
}
