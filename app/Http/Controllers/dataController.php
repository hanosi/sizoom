<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rules\Enum;
use phpDocumentor\Reflection\Types\Null_;
use Illuminate\Support\Facades\Auth;

class dataController extends Controller
{
    public function listPeminjaman()
    {
    	// mengambil data dari table peminjaman
    	$data = DB::table('data_peminjaman')
            ->join('users', 'data_peminjaman.no_identitas', '=', 'users.no_identitas')
            ->join('akun_zoom', 'akun_zoom.id_zoom', '=', 'data_peminjaman.id_zoom')
            ->get();
 
    	// mengirim data ke view index
    	return view('/fakultas/data',['data' => $data]);
    }
    public function updateStatus(Request $request)
    {
        // update data
        DB::table('data_peminjaman')->where('id',$request->id)->update([
            'status'=> 'Disetujui',
            'updated_at'=> date("Y-m-d H:i:s"),
        ]);
        // alihkan halaman
        return redirect('/listPeminjaman');
    }

    public function tolakPeminjaman(Request $request)
    {
        // update data
        DB::table('data_peminjaman')->where('id',$request->id)->update([
            'status'=> 'Ditolak',
            'updated_at'=> date("Y-m-d H:i:s"),
        ]);
        // alihkan halaman
        return redirect('/listPeminjaman');
    }

    public function updateStatuss(Request $request)
    {
        // update data
        DB::table('data_peminjaman')->where('id',$request->id)->update([
            'status'=> 'Dibatalkan',
            'updated_at'=> date("Y-m-d H:i:s"),
        ]);
        // alihkan halaman
        return redirect('/listPeminjaman');
    }

    public function updateStatusUser(Request $request)
    {
        // update data
        DB::table('users')->where('id',$request->id)->update([
            'status_akun'=> 'Aktif',
            'updated_at'=> date("Y-m-d H:i:s"),
        ]);
        // alihkan halaman
        return redirect('/userList');
    }
    public function StatusUser(Request $request)
    {
        // update data
        DB::table('users')->where('id',$request->id)->update([
            'status_akun'=> 'Tidak Aktif',
            'updated_at'=> date("Y-m-d H:i:s"),
        ]);
        // alihkan halaman
        return redirect('/userList');
    }

    // method untuk hapus data
    public function hapus($id)
    {
        // menghapus data berdasarkan id yang dipilih
        DB::table('data_peminjaman')->where('id',$id)->delete();
            
        // alihkan halaman
        return redirect('/home');
    }

    // method untuk menampilkan view form tambah
    public function tambah()
    {
        
        $data = DB::table('akun_zoom')->get();
        // memanggil view tambah
        return view('action/peminjaman/create',['data' => $data]);
    
    }
    // method untuk insert data ke table peminjaman
    public function tambahData(Request $request)
    {
        
        // insert data ke table peminjaman
        DB::table('data_peminjaman')->insert([
            'id' => Null,
            'no_identitas' => Auth::user()->no_identitas,
            'nama_kegiatan' => $request->nama_kegiatan,
            'deskripsi' => $request->deskripsi,
            'start' => $request->start,
            'end' => $request->end,
            'id_zoom' => $request->id_zoom,
            'created_at'=> date("Y-m-d H:i:s"),
            'updated_at'=> date("Y-m-d H:i:s"),
        ]);

        return redirect('/DataPeminjaman');
    }
}
